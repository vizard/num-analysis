#!/usr/bin/env python3

# Практическая работа 1: подварианты 1/2

# Подключаемые модули #
from math import sqrt                  # Функция квадратного корня
import sys                             # Для системных операций (чтение параметров программы, обработка ошибок и др.)
from py_expression_eval import Parser  # Для обработки заданных в текстовом виде формул

# Глобальные константы #
STANDARD_GAUS = 1     # Классический метод Гаусса
MODIFIED_GAUS = 2     # Модифицированный метод Гаусса (с поиском ведущего элемента по строке)
RELAXATION = 3        # Метод релаксации
FLOAT_ACCURACY = 5    # Точность представления результата (в разрядах после запятой)
REL_ACCURACY = 0.00001   # Точность метода релаксации
REL_ITERATIONS = 5000    # Число итераций в методе релаксации
W_INIT = 0.2          # Начальное значение итерационного параметра
W_FIN = 1.8           # Конечное значение итерационного параметра
W_STEP = 0.2          # Шаг изменения итерационного параметра
OTHER_PARAM = True    # Вычисление и вывод прочих значений (определитель, обратная матрица, число обусловленности)

# Точка входа программы #
def main(argv=sys.argv):
    input_wide_matrix = []      # Расширенная матрица рассматриваемой СЛАУ
    input_matrix_size = 0       # Порядок системы
    sol_method = STANDARD_GAUS  # Метод решения СЛАУ (см. глоб. константы)
    argv_size = len(argv)
    argv_ind = 1

    # Анализ аргументов командной строки #

    if argv_size <= 1 or (argv[1] != "--size" and argv[1] != "-s"):
        error("main: " + "Не задан размер матрицы!")
    while argv_ind < argv_size:
        arg = argv[argv_ind]
        if arg == "--size" or arg == "-s":
            if argv_ind == len(argv) - 1:
                error("main: " + "Некорректные аргументы командной строки!")
            argv_ind += 1
            input_matrix_size = int(argv[argv_ind])
        elif arg == "--method" or arg == "-m":
            if argv_ind == len(argv) - 1:
                error("main: " + "Некорректные аргументы командной строки!")
            argv_ind += 1
            if argv[argv_ind] == "MOD":
                sol_method = MODIFIED_GAUS
            elif argv[argv_ind] == "STD":
                sol_method = STANDARD_GAUS
            elif argv[argv_ind] == "REL":
                sol_method = RELAXATION
            else:
                error("main: " + "Некорректное значение метода!")
        elif arg == "--file" or arg == "-f":
            if argv_ind == len(argv) - 1:
                error("main: " + "Некорректные аргументы командной строки!")
            argv_ind += 1
            file_name = argv[argv_ind]
            try:
                file = open(file_name)
            except:
                error("main:" + "Ошибка при открытии файла!")
            if sum([1 for line in file]) != input_matrix_size:
                file.close()
                error("main: " + "Матрица не соответствует заявленному размеру!")
            file.seek(0)
            for line in file:
                values = [float(number) for number in line.split()]
                if len(values) != (input_matrix_size + 1):
                    file.close()
                    error("main: " + "Матрица не соответствует заявленному размеру!")
                input_wide_matrix.append(values)
            file.close()
        elif arg == "--formula" or arg == "-F":
            print("Используйте:\n'i' и 'j' в качестве номера строки и столбца элемента матрицы\n" +
                  "'m' - в качестве параметра-константы\n" +
                  "+, -, *, /, ^ - в качестве соответствующих операций")
            formula_nodiag = input("Введите формулу для внедиагональных элементов: ")
            formula_diag = input("Введите формулу для диагональных элементов: ")
            formula_rpart = input("Введите формулу для элементов правой части: ")
            param_m = int(input("Введите значение параметра m: "))
            input_wide_matrix = formula_gen(input_matrix_size, param_m, formula_nodiag, formula_diag, formula_rpart)
        else:
            error("main: " + "Некорректные аргументы командной строки!")
        argv_ind += 1

    # Решение СЛАУ и вычисление прочих искомых величин #
    print("Исходная матрица:\n")
    for row in input_wide_matrix:
        for elem in row:
            print("%9f" % (round_float(elem, FLOAT_ACCURACY)), end=" ")
        print("\n")
    if sol_method == STANDARD_GAUS or sol_method == MODIFIED_GAUS:  # Метод Гаусса
        result_vector = [round_float(x, FLOAT_ACCURACY) for x in gauss(input_wide_matrix, sol_method)] # Решение системы
        print("x = (", end="")                                      # Вывод
        for x in range(0, len(result_vector) - 1):
            print(result_vector[x], end="; ")
        print(result_vector[-1], end=")\n\n")
    elif sol_method == RELAXATION:     # Метод релаксации
        omega = W_INIT                 # Итерационный параметр
        best_omega = None              # Итерационный параметр с лучшей скоростью сходимости
        min_iter = REL_ITERATIONS + 1  # Минимальное достигнутое число итераций
        x = []  # Решение
        while omega < W_FIN:      # Перебор итерационных параметров
            result = relaxation(input_wide_matrix, omega, REL_ITERATIONS, REL_ACCURACY)
            if result == None:
                result = [None, "Расходится"]
            elif result[1] < min_iter:
                min_iter = result[1]
                x = result[0]
                best_omega = omega
            print("w =", round_float(omega, FLOAT_ACCURACY), "Итерации =", result[1])
            omega += W_STEP
        if best_omega == None:
            print("Метод расходится при всех значениях w")
        else:
            print("Значение итерационного параметра с лучшей сходимостью:", best_omega)
            print("Число итераций:", min_iter);
            x = [round_float(i, FLOAT_ACCURACY) for i in x]
            print("x = (", end="")                                      # Вывод
            for x_ind in range(0, len(x) - 1):
                print(x[x_ind], end="; ")
            print(x[-1], end=")\n\n")
    if OTHER_PARAM:
        input_matrix = wide_to_main_matrix(input_wide_matrix)
        print("Определитель:", round_float(det(input_matrix), FLOAT_ACCURACY),"\n")
        print("Обратная матрица:\n")
        for row in invert(input_matrix):
            for elem in row:
                print("%9f" % (round_float(elem, FLOAT_ACCURACY)), end=" ")
            print("\n")
        print("Число обусловленности:", round_float(cond_number(input_matrix), FLOAT_ACCURACY))



#''''''''''''''''''#
# Основные функции #
#..................#

# Решение СЛАУ методом Гаусса (классический и модифицированный - с поиском ведущего элемента по строке) #
def gauss(input_w_matrix, method):
    w_matrix = [row[:] for row in input_w_matrix] # Расширенная матрица СЛАУ
    matrix_order = len(w_matrix)                  # Порядок матрицы СЛАУ
    changes = []                                  # Перестановки столбцов (для модиф. метода)

    # Прямой ход #

    for step in range(0, matrix_order):
        main_elem = 0                     # Ведущий элемент текущего шага
        if method == MODIFIED_GAUS:       # Поиск ведущео элемента для модиф. метода
            max_in_row_ind = 0            # Индекс максимального элемента в первой строке рассматриваемого минора
            for column in range(step, matrix_order):
                if w_matrix[step][column] > w_matrix[step][max_in_row_ind]:
                    max_in_row_ind = column
            if w_matrix[step][max_in_row_ind] == 0:     # Нулевая строка - вырожденная матрица
                error("gauss: " + "Вырожденная матрица!")
            if max_in_row_ind != step:                  # Если найденный элемент не первый - перестановка столбцов
                changes.append((step, max_in_row_ind))  # Запись перестановки для восстановления порядка неизвестных
                for row in range(0, matrix_order):
                    w_matrix[row][step], w_matrix[row][max_in_row_ind] \
                            = w_matrix[row][max_in_row_ind], w_matrix[row][step]
            main_elem = w_matrix[step][step]
        elif method == STANDARD_GAUS:      # Поиск ведущео элемента для классич. метода
            main_elem = w_matrix[step][step]
            if main_elem == 0:             # Поиск ненулевого элемента по строкам в случае нулевого ведущего
                for new_main_row in range (step + 1, matrix_order):
                    if w_matrix[new_main_row][step] != 0:
                        w_matrix[step], w_matrix[new_main_row] = w_matrix[new_main_row], w_matrix[step]
                        main_elem = w_matrix[step][step]
                        break
                if main_elem == 0:                      # Ведущий элемент не найден - вырожденная матрица
                    error("gauss: " + "Вырожденная матрица!")
        else:
            error("gauss: " + "Некорректный параметр method!")
        
        for column in range(step, matrix_order + 1):    # Делим первую строку минора на ведущий элемент
            w_matrix[step][column] /= main_elem
        for row in range(step + 1, matrix_order):       # Обнуляем элементы под ведущим
            elem_to_remove = w_matrix[row][step]        # Коэффициент умножения
            for column in range(step, matrix_order + 1):
                w_matrix[row][column] -= w_matrix[step][column] * elem_to_remove

    x_vec = [0] * matrix_order  # искомый вектор неизвестных
    
    # Обратный ход #

    for row in range(matrix_order - 1, -1, -1):
        x = w_matrix[row][matrix_order]
        for other_x in range(row + 1, matrix_order):
            x -= w_matrix[row][other_x] * x_vec[other_x]
        x_vec[row] = x

    # Восстановление порядка столбцов #

    changes.reverse()
    for (first_row, second_row) in changes:
        x_vec[first_row], x_vec[second_row] = x_vec[second_row], x_vec[first_row]
    return x_vec


# Метод релаксации #
def relaxation(w_matrix, omega, iter_limit, epsilon):
    matrix_order = len(w_matrix)  # Порядок матрицы СЛАУ
    x_prev = [0] * matrix_order   # Значения предшествующей итерации
    x = [0] * matrix_order        # Значение текущей итерации
    is_ok = False     # Флаг достижения требуемой точности
    iter_counter = 0  # Счетчик итераций
    while (iter_counter < iter_limit and (not is_ok)):  # Итерации до получения результата с заданной точностью
                                                        # или исчерпания лимита итераций (в таком случае считаем
                                                        # метод расходящимся)
        try:
            x, x_prev = x_prev, x  # Смена векторов
            for x_elem_ind in range(0, matrix_order):  # Вычисление текущей итерации
                summary = 0
                summary -= sum([w_matrix[x_elem_ind][j]*x[j] for j in range(0, x_elem_ind)])
                summary -= sum([w_matrix[x_elem_ind][j]*x_prev[j] for j in range(x_elem_ind, matrix_order)])
                summary += w_matrix[x_elem_ind][-1]
                summary *= omega 
                summary /= w_matrix[x_elem_ind][x_elem_ind]
                x[x_elem_ind] = x_prev[x_elem_ind] + summary
            if (rate(x, x_prev, matrix_order) <= epsilon):  # Проверка на достижение искомой точности (через норму разности
                                                            # последовательных итераций)
                is_ok = True
        except OverflowError:
            return None
        iter_counter += 1  # Изменение счетчика итераций
    if iter_counter == iter_limit: # Если произошел выход за лимит итераций -
                                   # возвращаем пустое значене (метод расходится)
        return None
    else:  # Возвращаем пару значений - вектор неизвестных и число выполненных итераций для сравнения эффективности
        return (x, iter_counter)


# Определитель матрицы #
def det(input_matrix):
    matrix = [row[:] for row in input_matrix]
    det = 1                                # Определитель
    matrix_order = len(matrix)             # Порядок матрицы
    for step in range(0, matrix_order):

        # Выбр ведущего элемента (аналогично классическому методу Гаусса) #

        main_elem = matrix[step][step]
        if main_elem == 0:
            for new_main_row in range (step + 1, matrix_order):
                if matrix[new_main_row][step] != 0:
                    matrix[step], matrix[new_main_row] = matrix[new_main_row], matrix[step]
                    main_elem = matrix[step][step]
                    det *= -1
                    break
            if main_elem == 0: # Вырожденная матрица
                return 0
        det *= main_elem  # Домножение определителя во избежание его изменения

        # Прямой ход метода Гаусса (приведение матрицы к нижней треугольной форме с единичной диагональю) #

        for column in range(step, matrix_order):
            matrix[step][column] /= main_elem
        for row in range(step + 1, matrix_order):
            elem_to_remove = matrix[row][step]
            for column in range(step, matrix_order):
                matrix[row][column] -= matrix[step][column] * elem_to_remove
    return det  # Определитель полученной матрицы равен единице => накопленное значение det является искомым


# Обратная матрица (метод Гаусса-Жордана)#
def invert(input_matrix):
    matrix_order = len(input_matrix)  # Порядок матрицы

    # Матрица, полученная приписанием справа к исходной матрице единичной матрицы того же порядка
    g_matrix = [([i for i in input_matrix[row]] + [0 for add in range (0, matrix_order)]) for row in range(0, matrix_order)]
    for row in range(0, matrix_order):
        g_matrix[row][matrix_order + row] = 1

    # Приведение левой части полученной матрицы (исходной матрицы) к единичной #

    # Обнуление поддиагональных элементов (прямой ход Гаусса) #
    for step in range(0, matrix_order):
        main_elem = g_matrix[step][step]
        if main_elem == 0:
            for new_main_row in range (step + 1, matrix_order):
                if g_matrix[new_main_row][step] != 0:
                    g_matrix[step], g_matrix[new_main_row] = g_matrix[new_main_row], g_matrix[step]
                    main_elem = g_matrix[step][step]
                    break
            if main_elem == 0:
                error("invert: " + "Вырожденная матрица!")
        for column in range(step, 2 * matrix_order):
            g_matrix[step][column] /= main_elem
        for row in range(step + 1, matrix_order):
            elem_to_remove = g_matrix[row][step]
            for column in range(step, 2 * matrix_order):
                g_matrix[row][column] -= g_matrix[step][column] * elem_to_remove
    # Обнуление наддиагональных элементов #
    for step in range(matrix_order - 1, -1, -1):
        for row in range(step - 1, -1, -1):
            elem_to_remove = g_matrix[row][step]
            for column in range(step, 2 * matrix_order):
                g_matrix[row][column] -= g_matrix[step][column] * elem_to_remove

    # Выделение правой части матрицы в качетсве искомой обратной #
    for row in range(0, matrix_order):
        g_matrix[row] = g_matrix[row][matrix_order:]
    return g_matrix


# Число обусловленности матрицы (через каноническую l-норму матрицы) #
def cond_number(input_matrix):
    cond = 1       # Число обусловленности заданной матрицы
    matrixes = []  # Список для хранения матрицы и обратной к ней
    matrixes.append([row[:] for row in input_matrix])
    matrixes.append(invert(input_matrix))
    for matrix in matrixes: # Вычисление l-нормы каждой матрицы и перемножение полученных значений
        cond *= lrate(matrix)
    return cond



#'''''''''''''''''''#
# Служебные функции #
#...................#

# Округление вещественных чисел #
def round_float(number, n):
    rounded_number = int(number * 10 ** (n + 1))
    if rounded_number % 10 >= 5:
        rounded_number += 10
    rounded_number //= 10
    return (rounded_number / (10 ** n))


# l-Норма матрицы #
def lrate(matrix):
    matrix_order = len(matrix)        # Порядок матрицы
    column_sums = [0] * matrix_order  # Список сумм элементов в столбцах
    for column in range(0, matrix_order):
        for row in range(0, matrix_order):
            column_sums[column] += abs(matrix[row][column])
    norm_val = max(column_sums)  # Возвращаеиое значение - наибольшая из полученных сумм
    return norm_val


# Норма разности векторов x и x_prev #
def rate(x, x_prev, x_len):
    return sqrt(sum([((x[j] - x_prev[j]) ** 2) for j in range(0, x_len)]))


# Преобразование расширенной матрицы СЛАУ в обычную #
def wide_to_main_matrix(input_w_matrix):
    main_part = [input_w_matrix[row][:-1] for row in range(0, len(input_w_matrix))]
    return main_part


# Функция генерации массива, заданного формулами (из варианта задания) #
def formula_example():
    n = 30
    m = 20
    matrix = [[0] * (n + 1) for i in range(0,n)]
    for i in range(1, n+1):
        matrix[i-1][-1] = m * i + n
        for j in range(1, n+1):
            if i!=j:
                matrix[i-1][j-1] = (i+j)/(m+n)
            else:
                matrix[i-1][j-1] = n + m**2 + j/m + i/n
    return matrix


# Функция генерации массива, заданного формулами #
def formula_gen(n, m, not_diag, diag, right_part):
    parser = Parser()
    matrix = [[0] * (n + 1) for i in range(0,n)]
    for i in range(1, n+1):
        matrix[i-1][-1] = parser.parse(right_part).evaluate({'n': n, 'm': m, 'i': i})
        for j in range(1, n+1):
            if i!=j:
                matrix[i-1][j-1] = parser.parse(not_diag).evaluate({'n': n, 'm': m, 'i': i, 'j': j})
            else:
                matrix[i-1][j-1] = parser.parse(diag).evaluate({'n': n, 'm': m, 'i': i, 'j': j})
    return matrix


# Обработка ошибок #
def error(message = "Ошибка!", code = -1):
    sys.stderr.write(message+"\n")
    sys.exit(code)

main()
